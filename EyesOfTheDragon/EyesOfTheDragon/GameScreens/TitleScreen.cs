﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary;

namespace EyesOfTheDragon.GameScreens
{
    public class TitleScreen : BaseGameState
    {
        Texture2D backgroundImage;

        public TitleScreen(Game1 game, GameStateManager manager) : base(game, manager)
        {
            
        }

        protected override void LoadContent()
        {
            ContentManager content = gameRef.Content;
            backgroundImage = content.Load<Texture2D>(@"Backgrounds\titlescreen");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            gameRef.spriteBatch.Begin();
            base.Draw(gameTime);
            gameRef.spriteBatch.Draw(backgroundImage, gameRef.ScreenRectangle, Color.White);
            gameRef.spriteBatch.End();
        }
    }
}
